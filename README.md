## full_oppo6769-user 10 QP1A.190711.020 68b77aba7cb33275 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: arrow_RMX2020-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: cb4a3381f1
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:11/RQ3A.210905.001/7511028:user/release-keys
- OTA version: 
- Branch: full_oppo6769-user-10-QP1A.190711.020-68b77aba7cb33275-release-keys
- Repo: realme_rmx2020_dump_32407


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
